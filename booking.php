<?php

require_once 'vendor/autoload.php';
require_once 'init.php';

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/booking', function ($request, $response, $args) {

    if (!isset($_SESSION['bikeUser'])) { 
        $response = $response->withStatus(403);
        $_SESSION['id'] = $_GET['id'];
        return $this->view->render($response, 'access_denied.html.twig');
    }

    return $this->view->render($response, 'booking.html.twig', ['currstation' => $_SESSION['id']]);
});

$app->post('/booking', function (Request $request, Response $response, $args) {

    if (!isset($_SESSION['bikeUser'])) { // refuse if user not logged in
        $response = $response->withStatus(403);
        return $this->view->render($response, 'access_denied.html.twig');
    }
    $stationid = $request->getParam('stationid');
    $timeout = $request->getParam('timeout');
    $timein = $request->getParam('timein');
    $biketype = $request->getParam('bikeType');
    $userid = $_SESSION['bikeUser']['userId'];

    $dateoutsrt = strtotime($timeout);
    $onlydateout = date('m/d/Y',  $dateoutsrt);
    $onlytimeout = date('H',  $dateoutsrt);

    $dateinsrt = strtotime($timein);
    $onlydatein = date('m/d/Y', $dateinsrt);
    $onlytimein = date('H', $dateinsrt);
    /*  echo  $onlydateout . '-----------';
    echo  $onlytimeout . '-----------';
    echo  $onlydatein . '-----------';
    echo  $onlytimein . '-----------'; */


    $errorList = array();
    if (!$stationid) {
        array_push($errorList, "A Station Must Be Selected");
        return $this->view->render($response, 'fillallfields.html.twig', ['errorlist' => $errorList]);
        $errorList = array();
    }
    if (!$timeout && !$timein &&  !$biketype) {
        array_push($errorList, "TimeOut and  TimeIn and BikeType Must Be Selected ");
        return $this->view->render($response, 'fillallfields.html.twig', ['errorlist' => $errorList]);
        $errorList = array();
    }
    if (!$timeout || !$timein) {
        array_push($errorList, "BikeType Must Be Selected ");
        $errorList = array();
    }
    if ($biketype && !$timeout || !$timein) {
        array_push($errorList, "TimeOut and  TimeIn  Must Be Selected");
        return $this->view->render($response, 'fillallfields.html.twig', ['errorlist' => $errorList]);
        $errorList = array();
    }
    if (!$biketype) {
        array_push($errorList, "Bike Type Must Be Selected");
        return $this->view->render($response, 'fillallfields.html.twig', ['errorlist' => $errorList]);
    }

    // calculate cost
    if ($biketype == "Regular") {
        $Price = 7;
    } else if ($biketype == "Scooter") {
        $Price = 9;
    } else {
        $Price = 10;
    }


   /*  $onlytimein = 
    $onlytimeout = */ 
   $caltime =  $onlytimein- $onlytimeout;
  /*  $caltime = strtotime($caltime); */
   $finalcost = $caltime * $Price ;
   /* print_r($onlytimein.'##');
   print_r($onlytimeout.'##');
  
   print_r($caltime.'##');
    print_r($finalcost.'##'); */

    $timeout = date("Y-m-d h:i:s", strtotime($timeout));
    $timein = date("Y-m-d h:i:s", strtotime($timein));

    $bikeid  = DB::queryFirstField("SELECT bikeId FROM bikes as bs WHERE
    bs.stationId  ='$stationid' AND  bs.bikeType = '$biketype'
    AND bs.bikeId NOT IN ( SELECT bikeId FROM bookings as bo
    WHERE bo.timeOut = '$timeout'  AND bo.timeIn =  '$timein')");


    if ($bikeid == null) {
        return $this->view->render($response, 'nobikeavailable.html.twig', ['onlytimeout' => $onlytimeout, 'biketype' => $biketype, 'stationid' => $stationid, 'onlydateout' => $onlydateout, 'onlytimein' =>  $onlytimein]);
    } else {
      /*   $timeout = date("Y-m-d h:i:s", strtotime($timeout));
        $timein = date("Y-m-d h:i:s", strtotime($timein)); */
        DB::insert('bookings', ['userId' => $userid, 'bikeId' => $bikeid, 'timeIn' => $timein, 'timeOut' => $timeout, 'stationId' => $stationid,'payment' => $finalcost]);
        /*   unset($_SESSION['id']); */
        return $this->view->render($response, 'modal.html.twig', ['timeout' => $timeout, 'biketype' => $biketype, 'stationid' => $stationid, 'timein' =>   $timein,  $stationid, 'price' => $Price,'finalcost' =>  $finalcost ,'caltime' =>  $caltime ]);
    };



    /* if ($biketype != null){ */


    /*  return $this->view->render($response, 'modal.html.twig',['timeout' => $timeout, 'biketype' => $biketype,'stationid' => $stationid, 'timein' =>   $timein]);  */
    /*  } */

    /*  if( isset( $_GET['payid']) ){
      DB::insert('bookings', ['userId' => $userid, 'bikeId' => $bikeid , 'timeIn' => $timein, 'timeOut' => $timeout , 'stationId' => $stationid]);
 } */



    /* 
   return $response->write( $bikeid . "  " .$timeout . " _ " . $timein . " _ " . $biketype. " _ " . $stationid  );  */
});






$app->get('/test', function ($request, $response, $args) {

    return $response->withRedirect('/viewusers');
});
